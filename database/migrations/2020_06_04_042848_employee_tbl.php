<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EmployeeTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_tbl', function (Blueprint $table) {
            $table->integer('employee_id');
            $table->text('name');
            $table->integer('belong_id');
            $table->text('mail_address');
            $table->text('password');
            $table->text('manager');
            $table->integer('deleted_datetime');
            $table->text('image_file_path');
            // 主キー制約追加
            $table->primary('employee_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
