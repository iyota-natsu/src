<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $timestamps = false;
    protected $table = 'employee_tbl';
    //データの挿入ができるように設定
    protected $fillable = ['name','mail_address','password','belong_id','management'];

}
