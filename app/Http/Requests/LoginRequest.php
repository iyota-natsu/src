<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

/**
 * @method somethingElseIsInvalid()
 */
class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
    //認証の仕組みがない場合は、なんでも通すという意味で、trueを設定
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    //バリデーションはrulesメソッドに書く
    public function rules()
    {
        return [
         //バリデーションルール
            'mail_address' => 'required|email',
            'password' => 'required',
        ];
    }
    //日本語の表記
    public function attributes()
    {
        return [
            'mail_address' => 'メールアドレス',
            'password' => 'パスワード'
        ];
    }

}
