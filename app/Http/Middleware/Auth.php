<?php

namespace App\Http\Middleware;

use Closure;

class Auth
{
    protected $session;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    /*
     * セッションが確認できるとき、次の画面に移行します。
     */
    public function handle($request, Closure $next)
    {
        $exists = session()->exists('user');
        if($exists) {
            return $next($request);
        }
        else {
            //セッションの破棄をして、ログイン画面へ移行します。
            $this->session = session()->flush();
            return redirect()->to('/login');
        }
    }
}
