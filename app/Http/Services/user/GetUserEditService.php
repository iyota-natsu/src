<?php

namespace App\Http\Services\user;

use App\Queries\UserQuery;
use Illuminate\Support\Facades\DB;
use  Illuminate\Database\Query\Builder;

class GetUserEditService
{
    protected $belongs,$user,$user_query;
    /*
     * 初期設定
     */
    public function init()
    {
      $this->user_query = new UserQuery;
    }

    /*
     * サービスの処理を実行します。
     */
    //データを取得します。
    public function execute()
    {
        //所属課の情報の取得
        $this->belongs = $this->user_query->getBelong();
        //$idの値を取得
        $id = request('employee_id');
        //ユーザーリストの情報を呼び出します。
        $this->user = $this->user_query->getUserEdit($id);

    }

    //viewを返します。
    public function getBelong()
    {
        return $this->belongs;
    }

    //viewを返します。
    public function getUserEdit()
    {
        return $this->user;
    }

}

