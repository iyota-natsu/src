<?php

namespace App\Http\Services\user;

use App\Queries\UserQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UserRequest;

class PostUserAddService
{
    protected $management, $register, $belongs, $user_query;

    /*
     * 初期設定
     */
    public function init()
    {
        $this->user_query = new UserQuery;
    }
    /*
     * データを取得します。
     */
    public function execute(UserRequest $request)
    {
        $this->register = $this->user_query->register($request);
    }
    //viewを返します。
    public function getBelong()
    {
        return $this->belongs;
    }
    //viewを返します。
    public function getManagement()
    {
        return $this->management;
    }
    //エラー時の処理
    public function error(UserRequest $request)
    {
        if (!isset($this->register)) {
            //エラー時に、新規登録画面へリダイレクトします。
            return redirect('/user_add')
                ->withErrors($request)
                ->withInput();
        }
    }
    //登録できた時の処理
    public function success()
    {
        //登録成功時にredirect to でユーザーリスト画面に行く
        return redirect()->to('/user_list');
    }
}

