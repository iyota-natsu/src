<?php

namespace App\Http\Services\user;

use App\Queries\UserQuery;
use Illuminate\Support\Facades\DB;
use  Illuminate\Database\Query\Builder;

class GetUserAddService
{
    protected $belongs,$user, $user_query;

    /*
     * 初期設定
     */
    public function init()
    {
        $this->user_query = new UserQuery;
    }

    /*
     * サービスの処理を実行します。
     */
    //データを取得します。
    public function execute()
    {
        //所属課の情報の取得
        $this->belongs = $this->user_query->getBelong();
    }

    //viewを返します。
    public function getBelong()
    {
        return $this->belongs;
    }
}
