<?php

namespace App\Http\Services\user;

use App\Queries\UserQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UserRequest;

class PostUserEditService
{
    /*
     * クラス変数を定義します。
     */
    protected $validator,$management, $edit,$user_query;

    /*
     * 初期設定
     */
    public function init()
    {
        $this->user_query = new Userquery;
    }
    /*
     * データを取得します。
     */
    public function execute(UserRequest $request)
    {
        $this->edit = $this->user_query->getEdit($request);
    }
    //エラー時の処理
    public function error()
    {
        if (!isset($this->edit)) {
            //エラー時の処理
            return redirect('/user_edit')
                ->withErrors($this->validator)
                ->withInput();
        }
    //エラーでなければ、データを上書きしてユーザーリストへ行きます。
    }
    //viewを返します。
    public function getManagement()
    {
        return $this->management;
    }
    //viewを返します。
    public function getEdit()
    {
        return $this->edit;
    }
}
