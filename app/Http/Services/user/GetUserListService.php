<?php

namespace App\Http\Services\user;

use App\Queries\UserQuery;
use Illuminate\Support\Facades\DB;
use  Illuminate\Database\Query\Builder;

class GetUserListService
{
    protected $users, $belongs,$user_query;

    /*
     * 初期設定
     */
    public function init()
    {
    //インスタンスを作成して、クエリからデータを取得できるようにします。
        $this->user_query = new UserQuery();
    }
    /**
     * サービスの処理を実行します
     * @return mixed|void
     * @throws \Exception
     */
    //クエリからデータを取ってきます。
    public function execute()
    {
        //検索条件を画面から取得します()
        $keyId = request('keyId');
        $keyName = request('keyName');
        $belong = request('belong');
        //所属課の情報の取得
        $this->belongs = $this->user_query->getBelong();
        //ユーザーリストの情報を呼び出します。
        $this->users = $this->user_query->getUser($keyId,$keyName,$belong);
    }

    //viewを返します。
    public function getUser()
    {
        return $this->users;
    }

    //viewを返します。
    public function getBelong()
    {
        return $this->belongs;
    }
}
