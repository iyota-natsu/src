<?php

namespace App\Http\Services\user;

use App\Queries\UserQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostUserDeleteService
{
    /*
     * クラス変数の定義をします。
     */
    protected $user_query,$delete;
    /*
     * 初期設定
     */
    public function init()
    {
        $this->user_query = new UserQuery;
    }
    /*
     * データを取得します。
     */
    public function execute($id)
    {
        //削除ボタンを押したとき、削除日時が上書きされます。
        $this->delete = $this->user_query->deletedTime($id);
    }
}
