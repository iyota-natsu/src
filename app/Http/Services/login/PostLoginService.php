<?php

namespace App\Http\Services\login;

use App\Queries\LoginQuery;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;

class PostLoginService
{
    protected $login_query;
    public $login_user;
    public $validator;

    /*
     * 初期設定
     */
    public function init()
    {
    //インスタンスを作成して、クエリからデータを取得できるようにします。
        $this->login_query = new LoginQuery;
    }

    /*
     * サービスの処理を実行します
     */
    public function execute(LoginRequest $request)
    {
     //クエリからデータを取得します。
        $this->login_user = $this->login_query->Login($request);
    }
}
