<?php

namespace App\Http\Controllers;

use App\Http\Services\login;
use App\Http\Services\login\PostLoginService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\LoginRequest;


/**
 * ログインのコントローラークラスです。
 * Class LoginController
 * @package App\Http\Controllers
 */
class LoginController extends Controller
{
    /**
     * ログイン画面の表示
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLogin()
    {
        //viewを返します。
        return view('login');
    }

    /**
     * ログイン時の処理
     * @param Request $request
     */
    public function PostLoginService(LoginRequest $request)
    {
        //サービスからデータを取ってきます。
        $service = new PostLoginService();
        $service->init();
        $service->execute($request);
        $login_user = $service->login_user;

        //ログインができるかを判定
        if ($login_user->isEmpty()) {
            //エラー時の処理
            return redirect('/login')
                ->withErrors('登録されているユーザーがいません。')
                ->withInput();
        }
        //ログイン可能であれば、
        //セッションスタートして、ログインユーザーのデータを取ってきます。
        else {
            session()->put('user', $login_user[0]);
        //ユーザーリスト画面に行きます。
            return redirect()->to('/user_list');
        }
    }

    /*
     * ログアウトした時の処理
     */
    public function PostLogoutService(Request $request)
    {
        //全データ削除して、ログイン画面へ移行します。
        session()->flush();
        return redirect()->to('/login');
    }
}
