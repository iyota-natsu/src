<?php

namespace App\Http\Controllers;

use App\Http\Services\user\GetUserAddService;
use App\Http\Services\user\GetUserEditService;
use App\Http\Services\user\GetUserListService;
use App\Http\Services\user\PostUserAddService;
use App\Http\Services\user\PostUserDeleteService;
use App\Http\Services\user\PostUserEditService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UserRequest;


/**
 * ユーザーリストのコントローラークラスです
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * ユーザーリスト表示
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showUserList()
    {
        return view('user_list');
    }
    //GetUserListServiceからコントローラーに返します。
    public function GetUserListService()
    {
        //サービスからデータを取ってきます。
        $service = new GetUserListService();
        $service->init();
        $service->execute();
        //サービスから$usersを返す
        $users = $service->getUser();
        //サービスから$belongsを返す
        $belongs = $service->getBelong();

        if ($users->isEmpty()) {
            //エラー時の処理
            return redirect('/user_list')
                ->withErrors('登録されているユーザーがいません。')
                ->withInput();
        }
        //viewを返します。
        return view('/user_list', compact('users', 'belongs'));
    }

    /**
     * 削除処理を行い、deleted_datetimeを追加します。
     * @param
     * @return \Illuminate\Http\RedirectResponse
     */
    public function PostUserDeleteService($id)
    {
        //サービスからデータを取ってきます。
        $service = new PostUserDeleteService();
        $service->init();
        $service->execute($id);
        //削除処理を行い、ユーザーリスト画面へ戻ります。
        return redirect()->to('/user_list');
    }

    /**
     * 新規登録画面の機能を追加します。
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function GetUserAddService(Request $request)
    {
        //サービスからデータを取ってきます。
        $service = new GetUserAddService();
        $service->init();
        $service->execute();
        $belongs = $service->getBelong();
        //新規登録画面の表示をします。
        return view('/user_add', compact('belongs'));
    }

    /*
     * 登録ボタンを押すと、データベースに追加されます。
     */
    public function PostUserAddService(UserRequest $request)
    {
        //サービスからデータを取ってきます。
        $service = new PostUserAddService();
        $service->init();
        $service->execute($request);
        $management = $service->getManagement();
        $belongs = $service->getBelong();
        $error = $service->error($request);
        $success = $service->success();
        //エラー時の処理
        if ($error) {
            //エラー時の処理
            return redirect('/user_add')
                ->withErrors($request)
                ->withInput();
        }

        //新規登録ができたとき、redirect to でユーザーリスト画面に行く
        if ($success) {
            return redirect()->to('/user_list');
        }
    }

    /**
     * 編集画面の機能を追加します。
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function GetUserEditService()
    {
        //インスタンスを生成して、GetUserEditServiceからメソッドを呼び出します。
        $service = new GetUserEditService();
        $service->init();
        $service->execute();
        //変数を定義します。
        $belongs = $service->getBelong();
        $user = $service->getUserEdit();
        //viewを返します。
        return view('user_edit', compact('user', 'belongs'));
    }

    /*
     * 編集ボタンを押すと、データベース内の内容が編集されます。
     */
    public function PostUserEditService(UserRequest $request)
    {
        $service = new PostUserEditService();
        $service->init();
        $service->execute($request);
        $management = $service->getManagement();
        $edit = $service->getEdit();
        $error = $service->error();
        //エラー時の処理
        if ($error) {
            $service->error();
        //編集ができたとき、ユーザーリスト画面へ移行します。
        } else {
            return redirect()->to('/user_list');
        }
    }

    /* Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public
    function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public
    function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public
    function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
//    public
//    function edit(User $user)
//    {
//        //
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(User $user)
    {
        //
    }
}
