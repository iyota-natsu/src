<?php

namespace App\Queries;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\LoginRequest;

class LoginQuery
{
    public $validator;

    public function Login(LoginRequest $request)
    {
        //データを取得して、サービスに返します。
        $login_user = DB::table('employee_tbl')
            ->select('employee_tbl.*')
            ->where('mail_address', '=', $request->input('mail_address'))
            ->where('password', '=', $request->input('password'))
            ->get();
        return $login_user;
    }
}
