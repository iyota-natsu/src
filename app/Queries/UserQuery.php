<?php

namespace App\Queries;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UserRequest;

class UserQuery
{
    public $validator;
    /*
     * ユーザーリスト画面の情報を取得します。
     */
    //所属先の課の情報を取得します。
    public function getBelong()
    {
        //所属先のセレクトボックス内のデータを取得します。(ユーザーリスト、新規登録,編集画面)
        $belongs = DB::table('belong_master_tbl')
            ->select('belong_master_tbl.*')
            ->get();
        return $belongs;
    }

    //ユーザー情報の取得
    public function getUser($keyId,$keyName,$belong)
    {
        //検索条件をもとにDBからユーザー一覧取得
        $sql = DB::table('employee_tbl')
            ->select('employee_tbl.*', 'employee_tbl.deleted_datetime as delete', 'belong_master_tbl.name as group')
            ->join('belong_master_tbl', 'employee_tbl.belong_id',
                '=', 'belong_master_tbl.belong_id')
            ->where('deleted_datetime', '=', '');

        //検索条件のIDが入力されている時、where文追加
        if (!empty($keyId)) {
            $sql->where('employee_id', '=', $keyId);
        }
        //検索条件の名前が入力されている時、where文追加
        if (!empty($keyName)) {
            $sql->where('employee_tbl.name', 'LIKE', "%{$keyName}%");
        }
        //検索条件の所属先の課が入力されている時、where文追加
        if (!empty($belong)) {
            $sql->where('belong_master_tbl.belong_id', '=', $belong);
        }
        //ユーザーリストを管理者権限がある人から順に、また、ID順に並び変えます。
        $users = $sql->orderBy('employee_tbl.management', 'desc')
            ->orderBy('employee_tbl.employee_id', 'asc')
            ->get();
        return $users;
    }

    /*
     * 編集画面の情報取得
     */
    public function getUserEdit($id)
    {

        // 編集するユーザーの情報を取得します。
        $user = DB::table('employee_tbl')
            ->where('employee_tbl.employee_id', '=', $id)
            ->first();
        return $user;
    }

    /*
     * PostUserAddServiceの情報取得
     */
    public function register(Request $request)
    {
        //NULLが送信されるので、明示的に空文字をコントローラーに代入します。
        $management = $request->input('management');
        if (empty($management)) {
            $management = "";
        }
        //入力内容をデータベースに保存
        $register = DB::table('employee_tbl')->insert(
            ['name' => $request->input('name'),
                'mail_address' => $request->input('mail_address'),
                'password' => $request->input('password'),
                'management' => $management,
                'belong_id' => $request->input('belong'),
                'deleted_datetime' => "",
                'image_file_path' => "",
            ]);
        return $register;
    }

    /*
     * PostUserDeleteServiceの情報取得
     */
    public function deletedTime($id)
    {
        //削除ボタンを押したとき、削除日時が上書きされます。
        $delete = DB::table('employee_tbl')
            ->where('employee_id', '=', $id)
            ->update(['deleted_datetime' => now()]);
        return $delete;
    }

    /*
     * PostUserEditServiceの情報取得
     */
    public function getEdit(Request $request)
    {
        //NULLが送信されるので、明示的に空文字をコントローラーに代入します。
        $management = request('management');
        if (empty($management)) {
            $management = "";
        }
        $id = request('employee_id');
        //入力内容をデータベースに保存
        $edit = DB::table('employee_tbl')
            ->where('employee_id', '=', $id)
            ->update(
                ['name' => request('name'),
                    'mail_address' => request('mail_address'),
                    'password' => request('password'),
                    'management' => $management,
                    'belong_id' => request('belong'),
                    'deleted_datetime' => "",
                    'image_file_path' => "",
                ]);
        return $edit;
    }


}
