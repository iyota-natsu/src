<?php

//ログインフォーム
Breadcrumbs::for('login',function($trail) {
    $trail->push('ログイン',url('login'));
});

////ログイン　>　ユーザーリスト一覧
Breadcrumbs::for('user_list',function($trail) {
    $trail->parent('login');
    $trail->push('ユーザーリスト一覧',url('/user_list'));
});

///ログイン　>　ユーザーリスト一覧
Breadcrumbs::for('user_add',function($trail) {
    $trail->parent('user_list');
    $trail->push('新規登録画面',url('/user_add'));
});

///ログイン　>　ユーザーリスト一覧
Breadcrumbs::for('user_edit',function($trail) {
    $trail->parent('user_list');
    $trail->push('編集画面',url('/user_edit'));
});
