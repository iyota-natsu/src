<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//ログイン画面のルーティング
Route::get('/login', 'LoginController@showLogin'
)->name('login');

//ログイン時のルーティング
Route::post('/login', 'LoginController@PostLoginService'
)->name('login');

//ログアウトした時のルーティング
Route::post('/login/logout', 'LoginController@PostLogoutService'
)->name('logout');

//ユーザーリスト表示時のルーティング
Route::get('/user_list', 'UserController@GetUserListService'
)->name('user_list')->middleware(Auth::class);

//ユーザーリストの削除機能のルーティング
Route::post('/user_list/delete/{id}', 'UserController@PostUserDeleteService'
);

//新規登録画面のルーティング
Route::get('/user_add', 'UserController@GetUserAddService'
)->name('user_add')->middleware(Auth::class);

//新規登録ボタンを押したときのルーティング
Route::post('/user_add/register', 'UserController@PostUserAddService'
)->name('user_Add');

//編集画面のルーティング
Route::get('/user_edit', 'UserController@GetUserEditService'
)->name('user_edit')->middleware(Auth::class);

//編集ボタンを押したときのルーティング
Route::post('/user_edit/edit', 'UserController@PostUserEditService'
)->name('edit');
