<link rel="stylesheet" href="{{asset('css/login.css')}}">
{{--ベースブレイドの継承--}}
@extends("base")
{{--ベースレイアウトに挿入するコンテンツの定義--}}
@section("content")
    {{--エラー時のアラートを表示します。--}}
    @if ($errors->any())
        <div class="errors">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="body">
    {{--ログイン画面の入力フォームを表示します。--}}
        <form action="{{url('/login')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <p class="p_mail">
                メールアドレス
            </p>
            <label>
                <input type="text" name="mail_address" placeholder="e-mail@com">
            </label>
            <p>パスワード</p>
            <label>
                <input class="password" name="password" type="password" placeholder="password">
            </label>
            <label>
                <p class="checkbox"><input type="checkbox">ログイン情報をブラウザに保存</p></label>
            {{--ログインボタンを押すとユーザーリストに飛びます。--}}
            <button type="submit" class="submit">ログイン</button>
        </form>
    </div>
@endsection
