<link rel="stylesheet" href="{{asset('css/user_list.css')}}">
<script src={{asset('js/user_list.js')}}></script>
<link href="{{asset('provider/User.php')}}">
{{--ベースブレイドの継承--}}
@extends("base")
{{--ベースレイアウトに挿入するコンテンツの定義--}}
@section("content")
    <div class="wrapper">
        {{--ユーザーリスト画面のパンくず--}}
        {{ Breadcrumbs::render('user_list') }}
        {{--検索結果がない場合の処理----}}
        @if ($errors->any())
            <div class="errors">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {{--ユーザーリストを表示します。--}}
        <form action="{{url('/user_list')}}" method="GET">
            <ul class="form">
                <li>ID</li>
                <li class="keyId"><label for="id"></label><input type="text" id="id" name="keyId"
                                                                 value="{{$keyId ?? ''}}"></li>
                <li>名前</li>
                <li class="keyName"><label for="name"></label><input type="text" id="name" name="keyName"
                                                                     {{--belong_master_tblからデータを取ってきます。--}}                                                     value="{{$keyName ?? ''}}">
                </li>
                <li>所属</li>
                <li><label>
                        <select name="belong">
                            <option></option>
                            @foreach($belongs as $belong)
                                <option value="{{$belong->belong_id}}">{{$belong->name}}</option>
                            @endforeach
                        </select>
                    </label></li>
                {{--入力内容をクリアします。--}}
                <li>
                    <button type="reset">クリア</button>
                </li>
                {{--入力内容を検索します。--}}
                <li>
                    <button type="submit">検索</button>
                </li>
            </ul>
        </form>
        {{--管理者のみ新規登録ボタンが表示されます。--}}
        @if(session()->get('user')->management == 'あり')
            {{--新規登録ボタンを押すと新規登録画面へ移行します。--}}
            <div class="add">
                <a href="{{url('/user_add')}}">
                    <button type="button" class=" button icon-add">
                        <svg class="bi bi-person-plus" width="20px" height="15px" viewBox="0 0 16 16"
                             fill="currentColor"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                  d="M11 14s1 0 1-1-1-4-6-4-6 3-6 4 1 1 1 1h10zm-9.995-.944v-.002.002zM1.022 13h9.956a.274.274 0 0 0 .014-.002l.008-.002c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664a1.05 1.05 0 0 0 .022.004zm9.974.056v-.002.002zM6 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zm4.5 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                            <path fill-rule="evenodd"
                                  d="M13 7.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0v-2z"/>
                        </svg>
                        新規登録
                    </button>
                </a>
            </div>
        @endif
        {{--データベースからテーブルの中身を取ってきて表示します。--}}
        <table class="user-list">
            <tr>
                <th style="width:5%;">連番</th>
                <th style="width: 10%;">名前</th>
                <th style="width: 8%;">ID</th>
                <th style="width: 10%;">所属</th>
                <th style="width: 30%;">メールアドレス</th>
                <th style="width: 7%;">管理者</th>
                @if(session()->get('user')->management == 'あり')
                    <th style="width: 30%;">その他</th>
                @endif
            </tr>
            {{--データベースからの管理者のデータ--}}
            @foreach ($users as $user)
                @if($user->management == 'あり')
                    <tr class="management_line">
                @else
                    <tr class="user_line">
                        @endif
                        <td>
                            {{--現在の繰り返し数（1から開始)--}}
                            {{$loop->iteration}}
                        </td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->employee_id}}</td>
                        <td>{{$user->group}}</td>
                        <td>{{$user->mail_address}}</td>
                        <td>{{$user->management}}</td>
                        {{--管理者のみ削除ボタン、編集ボタンが表示されます。--}}
                        @if(session()->get('user')->management == 'あり')
                            <td>{{--削除ボタンの処理--}}
                                @if (session()->get('user')->employee_id !== $user->employee_id
                                        && $user->management == '')
                                    <form id="deleteForm" action="/user_list/delete/{{$user->employee_id}}"
                                          method="POST"
                                          name="deleteForm">
                                        {{ csrf_field() }}
                                        <button type="submit" data-open="exampleModal1"
                                                class="btn btn-outline-info float-left button icon-trash">
                                            <svg class="bi bi-trash" width="20px" height="15px"
                                                 viewBox="0 0 12 12"
                                                 fill="currentColor"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                <path fill-rule="evenodd"
                                                      d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                            </svg>
                                            削除
                                        </button>
                                    </form>
                                @endif
                                {{--編集ボタンをクリックすると、編集画面に移ります。--}}
                                <a href="/user_edit?employee_id={{$user->employee_id}}">
                                    <button class="btn btn-outline-info button icon-edit">
                                        <svg class="bi bi-wrench" width="20px" height="15px" viewBox="0 0 16 16"
                                             fill="currentColor"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                  d="M.102 2.223A3.004 3.004 0 0 0 3.78 5.897l6.341 6.252A3.003 3.003 0 0 0 13 16a3 3 0 1 0-.851-5.878L5.897 3.781A3.004 3.004 0 0 0 2.223.1l2.141 2.142L4 4l-1.757.364L.102 2.223zm13.37 9.019L13 11l-.471.242-.529.026-.287.445-.445.287-.026.529L11 13l.242.471.026.529.445.287.287.445.529.026L13 15l.471-.242.529-.026.287-.445.445-.287.026-.529L15 13l-.242-.471-.026-.529-.445-.287-.287-.445-.529-.026z"/>
                                        </svg>
                                        編集
                                    </button>
                                </a>
                            </td>
                        @endif
                    </tr>
                    @endforeach
        </table>
    </div>
@endsection




