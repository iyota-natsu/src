<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    {{--ビューポートの設定--}}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/base.css')}}">
{{--jquery,bootstrap--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
</head>
{{--ヘッダーの上の空間を作ります。--}}
<div class="header-separated"></div>
<body>
{{--ログイン画面の時はヘッダーを表示しない設定をします。--}}
@if(request()->route()->getName() !== 'login')
    {{--ヘッダーを表示します。--}}
    <header>
        <svg  class="bi bi-person-circle icon-user" width="3.5em" height="2.5em" viewBox="0 0 16 16" fill="currentColor"
             xmlns="http://www.w3.org/2000/svg">
            <path
                d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 0 0 8 15a6.987 6.987 0 0 0 5.468-2.63z"/>
            <path fill-rule="evenodd" d="M8 9a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
            <path fill-rule="evenodd" d="M8 1a7 7 0 1 0 0 14A7 7 0 0 0 8 1zM0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8z"/>
        </svg>
        {{session()->get('user')->name}}
{{--ログアウトボタン--}}
        <form action="{{url('/login/logout')}}" method="POST">{{ csrf_field() }}
            <button id="logout-button" type="submit">ログアウト</button>
        </form>
    </header>
@endif
{{--一番外枠のwrapperクラスの設定をします。--}}
<div class="wrapper">
{{--ユーザーリスト画面のパンくずを表示します。--}}
    @yield("content")
</div>
{{--ログイン画面時にはフッターを表示しない設定をします。--}}
@if(request()->route()->getName() !== 'login')
{{--フッターを表示します。--}}
    <footer>
        Copyright(C) Web. All Rights Researved.
    </footer>
@endif
{{--javascript--}}
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
@yield('script')
</body>
</html>
