<link rel="stylesheet" href="{{asset('css/user_edit.css')}}">
{{--ベースブレイドの継承--}}
@extends("base")
{{--ベースレイアウトに挿入するコンテンツの定義--}}
@section("content")
    <div class="wrapper">
    {{--パンくず--}}
        {{ Breadcrumbs::render('user_edit') }}
{{--エラー時のアラートを表示します。--}}
        @if ($errors->any())
            <div class="errors">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {{--フォームの入力画面--}}
        <form action="/user_edit/edit" method="POST">
            @csrf
        {{--フォームの入力画面--}}
            <ul class="form">
                <input type="hidden" name="employee_id" value="{{$employee_id=$user->employee_id}}">
                <li>名前</li>
                <li>
                    <label><input type="text" name="name" value="{{$name=$user->name}}"></label>
                </li>

                <li>メールアドレス</li>
                <li>
                    <label><input type="text" name="mail_address" value="{{$mail_address=$user->mail_address}}"></label>
                </li>

                <li>パスワード</li>
                <li>
                    <label><input type="password" name="password" value="{{$password=$user->password}}"></label>
                </li>
                {{--belong_master_tblからデータを取ってきます。--}}
                <li>所属</li>
                <li><label>
                        <select name="belong">
                            @foreach($belongs as $belong)
                                <option value="{{$belong->belong_id}}">{{$belong->name}}</option>
                            @endforeach
                        </select>
                    </label>
                </li>
                {{--管理者権限があるかどうかのデータを取ってきます。--}}
                <li>管理者権限</li>
                <li>
                    <div class=" radio btn-group btn-group-toggle radio" data-toggle="buttons">
                        <label class="btn btn-outline-secondary active">
                            <input type="radio" name="management" value="あり" id="option1" autocomplete="off" checked>あり
                        </label>
                        <label class="btn btn-outline-secondary">
                            <input type="radio" name="management" value=" " id="option2" autocomplete="off">なし
                        </label></div>
                </li>
                {{--戻るボタンを押すと、ユーザーリストへ戻る--}}
                <li>
                    <a href="{{url('/user_list')}}">
                        <button class="reset" type="button">戻る</button>
                    </a>
                    {{--編集ボタンを押すと、データが編集されます。--}}
                    <button class="edit" type="submit">編集</button>
                </li>
            </ul>
        </form>
    </div>
@endsection
